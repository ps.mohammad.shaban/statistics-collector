package com.progressoft.jip9;

/**
 * Defines the implementer as a statistics collector strategy which accepts a
 * list of objects, collect required information (statistics) from them, then
 * returns a list of all calculated statistics.
 */
public interface StatisticsCollector<T extends Object, S extends Statistic> {


    /**
     * Accept then collect statistics from passed <code>objects</code>.
     * <p>
     *
     * @param objects the objects to collect statistics from
     * @return calculated statistics
     */
    Iterable<S> collectStatistics(Iterable<T> objects);
}